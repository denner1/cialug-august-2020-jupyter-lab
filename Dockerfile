FROM ubuntu:20.04
RUN  apt-get update; DEBIAN_FRONTEND="noninteractive"  apt-get install -y  git r-base r-cran-devtools nodejs wget python3-pip build-essential libssl-dev libffi-dev python-dev python3-venv
RUN wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb; dpkg -i packages-microsoft-prod.deb
RUN pip3 install virtualenv 
RUN R -e 'devtools::install_github("IRkernel/IRkernel")'
RUN DEBIAN_FRONTEND="noninteractive"  apt install -y gnuplot